package CodingJavaAdvancedFeatures.Ex3;



/* Personal information
a. Create a file containing any personal data (name, surname, phone number). Data of individual persons should be in the following lines.
b. Download data from a file and create objects of people based on them (in any way - Regex, String.split ...).
c. Enter the created objects into ArrayList or Map (<line number>: <Person>).
d. Present the obtained data. */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        List<String> personList = readFile("personalData.txt");
        for (int i = 0; i < personList.size(); i++) {
            System.out.println(personList.get(i));
        }
        System.out.println("================");

        List<Person> personObjects = new ArrayList<>();
        Map<Integer, Person> personMap = new HashMap<>();

        int contor = 1;

        for (String person : personList) {
            // impartim elementele din String dupa ", " pentru a obtine un array de string-uri
            String[] data = person.split(", ");
            // folosind constructorul Person trimitem datele obtinute mai sus
            Person personObject = new Person(data[0], data[1], data[2]);
            // adaugam obiectele in lista de obiecte
            personObjects.add(personObject);
            personMap.put(contor, personObject);
            contor++;
            // System.out.println("Data: " + data[0] + " " + data[1] + " "+ data[2]);
            System.out.println(person);

        }

        System.out.println("~~~~~~~~~~~~~~~~~~~");

        for (int i = 0; i < personObjects.size(); i++) {
            System.out.println(personObjects.get(i));
        }
        System.out.println("*****************");
        // Printare Map
        personObjects.stream().forEach((person) -> System.out.println(person));

        System.out.println("%%%%%%%%%%%%%%%%%%");

        for(Map.Entry<Integer, Person> entry: personMap.entrySet()){
            System.out.println("ID: " + entry.getKey() + " -> " + entry.getValue());
        }

    }

    public static List<String> readFile(String filename) {

        List<String> personList = new ArrayList<>();
        try {
            personList = Files.readAllLines(Path.of("personalData.txt"));
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return personList;
    }


}
