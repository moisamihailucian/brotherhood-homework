package CodingJavaAdvancedFeatures.Ex1;

public class Developer extends Person {

    public Developer() {
        super();
        System.out.println("Constructor: Developer");
    }

    protected static void DoABackflip () {
        System.out.println("Me, Developer, I did a backflip");
    }
}
