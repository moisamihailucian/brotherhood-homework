package CodingJavaAdvancedFeatures.Ex1;

public class JavaDeveloper extends Developer{

    public JavaDeveloper() {
        super();
        System.out.println("Constructor: JavaDeveloper");

    }

    protected static void PersonMethod (String x, String y) {
        System.out.println("Me, Person, I did a method");
    }

}
