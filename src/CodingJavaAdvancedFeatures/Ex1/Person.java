package CodingJavaAdvancedFeatures.Ex1;

public class Person {

    public Person() {
        System.out.println("Constructor: Person");
    }

    protected static void PersonMethod (String x) {
        System.out.println("Me, Person, I did a method");
    }


}
