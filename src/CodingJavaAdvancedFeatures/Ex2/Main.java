package CodingJavaAdvancedFeatures.Ex2;

/*
 * Warehouse
 * a. User should be able to: add, display all of the details, update, delete an item
 * b. Use composition and collections (The warehouse has products/items)
 * c. Add possibility to display summaries, like sum of all of the products, their prices.
 * d. *Add possibility to update number of items in a specific way, e.g.: “pliers:30” “scissors:+4”
 */

public class Main {
    public static void main(String[] args) throws Exception {
        User user1 = new User();

        Item item1 = new Item(231, "Creioane", 20, 0.90);
        Item item2 = new Item(12, "Pixuri", 50, 1.20);
        Item item3 = new Item(543, "Stilouri", 10, 4.55);
        Item item4 = new Item(118, "Carioci", 40, 2.55);

        user1.addItem(item1);
        user1.addItem(item2);
        user1.addItem(item3);
        user1.addItem(item4);

        user1.displayAllItems();
        user1.displayItem(item1);

        System.out.println("----------------------------------------");

        item3.setCantitate(100);

        //item1.setName("Acuarele");
        try {
            user1.deleteItem(item1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        user1.displayAllItems();

        System.out.println("----------------------------------------");

        try {
            user1.displayItem(item2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("----------------------------------------");

        System.out.println("Pretul tuturor produselor este: " + user1.sumOfAllPrices());
        System.out.println("Numarul total al produselor este: " + user1.sumOfAllProducts());
    }
}
