package CodingJavaAdvancedFeatures.Ex2;

public class Item {
    private int id;
    private String itemName;
    private double pret;
    private int cantitate;

    public Item(int id, String itemName, int cantitate, double pret) {
        this.id = id;
        this.itemName = itemName;
        this.cantitate = cantitate;
        this.pret = pret;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public String getItemName() {
        return itemName;
    }

    public double getPret() {
        return pret;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getCantitate() {
        return cantitate;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", itemName='" + itemName + '\'' +
                ", pret=" + pret +
                ", cantitate=" + cantitate +
                '}';
    }
}
