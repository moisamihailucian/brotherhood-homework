package CodingJavaAdvancedFeatures.Ex2;

public interface ItemManager {

    void addItem(Item item);

    void editItem(Item item) throws Exception;

    void deleteItem(Item item) throws Exception;

    void displayItem(Item item) throws Exception;

    void displayAllItems();
}
