package CodingJavaAdvancedFeatures.Ex2;

public interface ItemSummary {

    int sumOfAllProducts();

    double sumOfAllPrices();
}
