package CodingJavaAdvancedFeatures.Ex2;

import java.util.HashMap;
import java.util.Map;

public class WarehouseManager implements ItemManager, ItemSummary {
    private Map<Integer, Item> itemsMap;

    public WarehouseManager() {
        this.itemsMap = new HashMap<>();
    }

    @Override
    public void addItem(Item item) {
        int key = item.getId();
        if (itemsMap.containsKey(key)) {
            Item oldItem = itemsMap.get(key);

            int oldCantitate = oldItem.getCantitate();
            int newCantitate = oldCantitate + item.getCantitate();

            oldItem.setCantitate(newCantitate);

            itemsMap.put(key, oldItem);
        } else {
            itemsMap.put(key, item);
        }
    }

    @Override
    public void editItem(Item item) throws Exception {
        int key = item.getId();
        if (itemsMap.containsKey(key)) {
            itemsMap.put(key, item);
        } else {
            throw new Exception("This element was not found");
        }
    }

    @Override
    public void deleteItem(Item item) throws Exception {
        int key = item.getId();
        if (itemsMap.containsKey(key)) {
            itemsMap.remove(key, item);
        } else {
            throw new Exception("This element was not found");
        }
    }

    @Override
    public void displayItem(Item item) throws Exception {
        int key = item.getId();
        if (itemsMap.containsKey(key)) {
            System.out.println(itemsMap.get(key));
        } else {
            throw new Exception("This element was not found");
        }

    }

    @Override
    public void displayAllItems() {
        for (Map.Entry<Integer, Item> x : this.itemsMap.entrySet()) {
            Item item = x.getValue();
            System.out.println(item.toString());
        }
    }

    @Override
    public int sumOfAllProducts() {
        int totalProducts = 0;
        for (Map.Entry<Integer, Item> x : this.itemsMap.entrySet()) {
            totalProducts += x.getValue().getCantitate();
        }
        return totalProducts;
    }

    @Override
    public double sumOfAllPrices() {
        int totalPrice = 0;
        for (Map.Entry<Integer, Item> x : this.itemsMap.entrySet()) {
            totalPrice += x.getValue().getPret() * x.getValue().getCantitate();
        }
        return totalPrice;
    }
}
